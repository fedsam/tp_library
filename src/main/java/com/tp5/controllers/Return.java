package com.tp5.controllers;

import com.tp5.dao.BorrowingDAO;
import com.tp5.models.Borrowing;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@WebServlet(urlPatterns = "/return")
public class Return extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer borrowingId = (!request.getParameter("borrowing_id").isEmpty()) ? Integer.parseInt(request.getParameter("borrowing_id")) : null;
        BorrowingDAO borrowingDAO = new BorrowingDAO();
        Borrowing borrow = borrowingDAO.get(borrowingId);

        borrow.setReturned(new Date());

        borrowingDAO.update(borrow);

        response.sendRedirect("/dashboard");
    }
}
