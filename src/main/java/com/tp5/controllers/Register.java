package com.tp5.controllers;

import com.tp5.dao.UserDAO;
import com.tp5.models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/register")
public class Register extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("title", "Créer un compte");
        this.getServletContext().getRequestDispatcher("/WEB-INF/jsp/register.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String lastname = request.getParameter("lastname");
        String firstname = request.getParameter("firstname");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String confirmed_password = request.getParameter("confirmed_password");

        if (password.equals(confirmed_password)) {
            User user = new User(null, firstname, lastname, email, password);
            UserDAO userDAO = new UserDAO();
            userDAO.create(user);
            response.sendRedirect("/login");
        } else {
            request.setAttribute("title", "Créer un compte");
            request.setAttribute("lastname", lastname);
            request.setAttribute("firstname", firstname);
            request.setAttribute("email", email);
            request.getRequestDispatcher("/WEB-INF/jsp/register.jsp").forward(request, response);
        }
    }
}
