package com.tp5.models;

import java.util.Date;

public class Borrowing {
    private Integer id;
    private User user;
    private Book book;
    private Date borrowed;
    private Date returned;

    public Borrowing(Integer id, User user, Book book, Date borrowed, Date returned) {
        this.id = id;
        this.user = user;
        this.book = book;
        this.borrowed = borrowed;
        this.returned = returned;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Date getBorrowed() {
        return borrowed;
    }

    public void setBorrowed(Date borrowed) {
        this.borrowed = borrowed;
    }

    public Date getReturned() {
        return returned;
    }

    public void setReturned(Date returned) {
        this.returned = returned;
    }
}
