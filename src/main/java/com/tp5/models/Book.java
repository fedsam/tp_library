package com.tp5.models;

import java.util.Date;

public class Book {
    private Integer id;
    private String title;
    private Date publication_date;
    private Type type;
    private Author author;

    public Book(Integer id, String title, Date publication_date, Type type, Author author) {
        this.id = id;
        this.title = title;
        this.publication_date = publication_date;
        this.type = type;
        this.author = author;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getPublication_date() {
        return publication_date;
    }

    public void setPublication_date(Date publication_date) {
        this.publication_date = publication_date;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
}
