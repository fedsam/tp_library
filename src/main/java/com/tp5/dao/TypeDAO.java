package com.tp5.dao;

import com.tp5.models.Type;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TypeDAO extends DAO<Type> {
    @Override
    public Type get(Integer id) {
        Type type = null;
        try (Connection connection = DriverManager.getConnection(dbUrl, dbLogin, dbPassword)) {
            final String SQL_QUERY = "SELECT * FROM types WHERE type_id = ?";
            try(PreparedStatement statement = connection.prepareStatement(SQL_QUERY)) {
                statement.setInt(1, id);
                try(ResultSet resultSet = statement.executeQuery()) {
                    if(resultSet.next()) {
                        type = new Type(
                                resultSet.getInt("type_id"),
                                resultSet.getString("name")
                        );
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return type;
    }

    @Override
    public List<Type> getAll() {
        List<Type> types = null;
        try (Connection connection = DriverManager.getConnection(dbUrl, dbLogin, dbPassword)) {
            final String SQL_QUERY = "SELECT * FROM types";
            try(PreparedStatement statement = connection.prepareStatement(SQL_QUERY)) {
                try(ResultSet resultSet = statement.executeQuery()) {
                    types = new ArrayList<Type>();

                    while(resultSet.next()) {
                        types.add(new Type(
                                resultSet.getInt("type_id"),
                                resultSet.getString("name")
                        ));
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return types;
    }

    @Override
    public void create(Type obj) {

    }

    @Override
    public void update(Type obj) {

    }

    @Override
    public void delete(Type obj) {

    }
}
