package com.tp5.dao;

import javax.servlet.ServletContext;
import java.util.List;

public abstract class DAO<T> {
    protected static String dbUrl;
    protected static String dbLogin;
    protected static String dbPassword;

    public static void init(ServletContext context) {
        try {
            Class.forName(context.getInitParameter("JDBC_DRIVER"));
            dbUrl = context.getInitParameter("JDBC_URL");
            dbLogin = context.getInitParameter("JDBC_LOGIN");
            dbPassword = context.getInitParameter("JDBC_PASSWORD");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public abstract T get(Integer id);
    public abstract List<T> getAll();
    public abstract void create(T obj);
    public abstract void update(T obj);
    public abstract void delete(T obj);
}
