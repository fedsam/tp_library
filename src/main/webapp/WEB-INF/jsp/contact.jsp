<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="layout" tagdir="/WEB-INF/tags" %>

<layout:layout>
    <jsp:body>
        <div class="wrapper">
            <h1>Contact</h1>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed sapien lobortis, ultricies arcu vel, ullamcorper felis. Phasellus in pulvinar elit. Suspendisse finibus nibh eu ex suscipit scelerisque. Quisque placerat dolor eu rhoncus dictum. In bibendum orci eget ligula pellentesque, quis venenatis lorem elementum. Fusce scelerisque malesuada velit at dapibus. Quisque at porttitor turpis. Vestibulum vitae condimentum justo.
                <br>
                Cras risus dui, finibus commodo lobortis ut, porttitor sed lorem. Sed sollicitudin blandit purus maximus tempus. Etiam euismod enim est, ac interdum dui tempus ut. In ullamcorper urna ante, eu vulputate sem imperdiet vel. Nunc ornare quam eget nunc ullamcorper, sit amet venenatis turpis placerat. Fusce massa nisl, porta eget lectus eget, lobortis pharetra tortor. Morbi mattis ligula eu ex lacinia blandit. Suspendisse ut efficitur felis.
                <br>
                Cras sollicitudin lorem efficitur, mollis tortor at, rhoncus lectus. In eu felis ut massa suscipit convallis quis vitae nibh. Morbi eu lacinia orci. In quis lectus sit amet lectus facilisis volutpat ut feugiat nisl. Donec nec gravida ex. Nunc tristique nulla eget orci aliquet porttitor. Cras hendrerit ex dui. Curabitur fringilla massa odio, posuere finibus lorem auctor consectetur.
            </p>
        </div>
    </jsp:body>
</layout:layout>